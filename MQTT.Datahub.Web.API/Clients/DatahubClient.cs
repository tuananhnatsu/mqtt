﻿using Microsoft.Extensions.Options;
using RestSharp;
using System;

namespace MQTT.Datahub.Web.API.Clients
{
    public class DatahubClient
    {
        private readonly RestClient _client;
        private readonly DatahubApiConfiguration _setting;

        public DatahubClient(IOptions<DatahubApiConfiguration> settings)
        {
            _setting = settings.Value;
            _client = new RestClient(_setting.BaseUrl);
        }

        // Sample
        //public async Task<ADTokenResponse> GetClientToken()
        //{
        //    if (ClientToken.TokenExpiry > DateTime.UtcNow)
        //    {
        //        return ClientToken;
        //    }
        //    var request = new RestRequest(_setting.TokenEndPoint);
        //    request.AddHeader("Content-Type", "application/x-www-form-urlencoded");
        //    request.AddParameter("clientId", _setting.ClientId);
        //    request.AddParameter("clientPassword", _setting.ClientPassword);
        //    var response = await _client.ExecuteGetAsync<ADTokenResponse>(request);
        //    ClientToken = response.Data;
        //    return ClientToken;
        //}

        public void Dispose()
        {
            _client?.Dispose();
            GC.SuppressFinalize(this);
        }
    }
}
