﻿using Microsoft.Extensions.Options;
using MQTT.Datahub.Web.API.BussinessLogic.Interfaces;
using MQTT.Device.DotNet.SDK;
using MQTT.Device.DotNet.SDK.Model;
using Newtonsoft.Json.Linq;
using System;
using System.Threading.Tasks;

namespace MQTT.Datahub.Web.API.BussinessLogic
{
    public class DatahubService : IDatahubService
    {
        private EdgeAgent _edgeAgent;
        private readonly EdgeAgentOptions _edgeAgentOptions;
        private readonly MQTTOptions _mqttOptions;

        public DatahubService(IOptions<EdgeAgentOptions> edgeAgentOptions, IOptions<MQTTOptions> mqttOptions)
        {
            _edgeAgentOptions = edgeAgentOptions.Value;
            _mqttOptions = mqttOptions.Value;
        }

        /// <summary>
        /// Inform the connection is active
        /// </summary>
        private void _edgeAgent_Connected(object sender, EdgeAgentConnectedEventArgs e)
        {
            IDatahubService.ConnectionStatus = "CONNECTED";
        }
        /// <summary>
        /// Inform the connection is restricted
        /// </summary>
        private void _edgeAgent_Disconnected(object sender, DisconnectedEventArgs e)
        {
            IDatahubService.ConnectionStatus = "DISCONNECTED";
        }
        private void _edgeAgent_MessageReceived(object sender, MessageReceivedEventArgs e)
        {
            switch (e.Type)
            {
                case MessageType.WriteValue:
                    WriteValueCommand wvcMsg = (WriteValueCommand)e.Message;
                    foreach (var device in wvcMsg.DeviceList)
                    {
                        foreach (var tag in device.TagList)
                        {
                            if (tag.Value.GetType() == typeof(JObject))
                            {
                                //Array Tag
                                JObject arrayTag = JObject.Parse(tag.Value.ToString());
                                string index = arrayTag.First.Path;
                                var value = arrayTag.First.First;
                                Console.WriteLine("TagName: {0}, Index: {1}, Value: {2}", tag.Name, index, value.ToString());
                            }
                            else
                            {
                                //Non-Array Tag
                                Console.WriteLine("TagName: {0}, Value: {1}", tag.Name, tag.Value.ToString());
                            }

                        }
                    }
                    break;
                /*case MessageType.WriteConfig:
                    break;*/
                case MessageType.TimeSync:  // when received this message
                    TimeSyncCommand tscMsg = (TimeSyncCommand)e.Message;
                    Console.WriteLine("UTC Time: {0}", tscMsg.UTCTime.ToString());
                    break;
                case MessageType.ConfigAck:
                    ConfigAck cfgAckMsg = (ConfigAck)e.Message;
                    Console.WriteLine(string.Format("Upload Config Result: {0}", cfgAckMsg.Result.ToString()));
                    break;
            }
        }
        public async Task ConnectAsync()
        {
            if (_edgeAgent == null)
            {
                EdgeAgentOptions options = _edgeAgentOptions;

                switch (options.ConnectType)
                {
                    case ConnectType.MQTT:
                        options.MQTT = _mqttOptions;
                        break;
                }

                _edgeAgent = new EdgeAgent(options);

                _edgeAgent.Connected += _edgeAgent_Connected;
                _edgeAgent.Disconnected += _edgeAgent_Disconnected;
                _edgeAgent.MessageReceived += _edgeAgent_MessageReceived;
            }
            await _edgeAgent.Connect();
        }
    }
}
