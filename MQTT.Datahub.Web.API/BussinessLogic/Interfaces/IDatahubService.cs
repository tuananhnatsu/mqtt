﻿using System.Threading.Tasks;

namespace MQTT.Datahub.Web.API.BussinessLogic.Interfaces
{
    public interface IDatahubService
    {
        public static string ConnectionStatus = "";
        Task ConnectAsync();
    }
}
