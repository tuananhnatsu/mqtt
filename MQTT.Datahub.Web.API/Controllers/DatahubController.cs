﻿using Microsoft.AspNetCore.Mvc;
using MQTT.Datahub.Web.API.BussinessLogic.Interfaces;
using System.Collections.Generic;
using System.Threading.Tasks;

// For more information on enabling Web API for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace MQTT.Datahub.Web.API.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class DatahubController : ControllerBase
    {
        private readonly IDatahubService _datahubService;
        public DatahubController(IDatahubService datahubService)
        {
            _datahubService = datahubService;
        }

        // POST api/<DatahubController>
        [HttpPost("connect")]
        public async Task<IActionResult> Connect()
        {
            await _datahubService.ConnectAsync();
            return Ok(IDatahubService.ConnectionStatus);
        }
    }
}
